# menu and user input part
import os
import getpass


class color_text():  # text formatting
    CBOLD = '\33[1m'
    CBLUE = '\33[34m'
    CGREEN = '\33[32m'
    CRED = '\033[91m'
    CEND = '\033[0m'
    CURL = '\33[4m'
    CBLINK = '\33[5m'
    CBLINK2 = '\33[6m'
    CSELECTED = '\33[7m'
    CBLACK = '\33[30m'
    CYELLOW = '\33[33m'
    CVIOLET = '\33[35m'
    CBEIGE = '\33[36m'
    CWHITE = '\33[37m'

    CBLACKBG = '\33[40m'
    CREDBG = '\33[41m'
    CGREENBG = '\33[42m'
    CYELLOWBG = '\33[43m'
    CBLUEBG = '\33[44m'
    CVIOLETBG = '\33[45m'
    CBEIGEBG = '\33[46m'
    CWHITEBG = '\33[47m'

    CGREY = '\33[90m'
    CRED2 = '\33[91m'
    CGREEN2 = '\33[92m'
    CYELLOW2 = '\33[93m'
    CBLUE2 = '\33[94m'
    CVIOLET2 = '\33[95m'
    CBEIGE2 = '\33[96m'
    CWHITE2 = '\33[97m'

    CGREYBG = '\33[100m'
    CREDBG2 = '\33[101m'
    CGREENBG2 = '\33[102m'
    CYELLOWBG2 = '\33[103m'
    CBLUEBG2 = '\33[104m'
    CVIOLETBG2 = '\33[105m'
    CBEIGEBG2 = '\33[106m'
    CWHITEBG2 = '\33[107m'

    def info(msg):  # GREEN
        print('\33[6m', '\33[1m', '\33[32m', msg, '\033[0m')

    def error(msg):  # RED
        print('\33[1m', '\033[91m', msg, '\033[0m')

    def warning(msg):  # YELLOW
        print('\33[1m', '\33[33m', msg, '\033[0m')

    def resume(msg):  # BLUE BLINK
        print('\33[6m', '\33[1m', '\33[104m', msg, '\033[0m')

    def change_to_green():
        print('\33[1;32;40m')

    def change_to_blue():
        print('\33[94m')

    def reset_style():
        print('\033[m')


os.system('clear')


def welcome():
    color_text.info("Welcome to Bitcoin Core + Lightning Network Daemon installation script")
    color_text.info("Please insert some info before installation")


class user_input():

    def check_answer(answer):
        a1 = answer[0].lower()
        if answer == '' or not a1 in ['y', 'n']:
            color_text.warning('Please answer y or n')
            return False
        else:
            return True

    def check_password(first_pass, confirm_pass):
        if first_pass == confirm_pass:
            return True
        else:
            return False

    def RPC_user():
        color_text.change_to_blue()
        RPC_user = input(' Insert RPC username ---> ')
        color_text.reset_style()
        return RPC_user

    def RPC_pass():
        color_text.change_to_blue()
        RPC_pass = getpass.getpass(prompt=' Insert RPC password ---> ')
        confirm_pass = getpass.getpass(prompt=' Repeat RPC password ---> ')
        color_text.reset_style()
        if user_input.check_password(RPC_pass, confirm_pass):
            return RPC_pass
        else:
            color_text.warning('Passwords don\'t match, try again.')
            return user_input.RPC_pass()
        color_text.reset_style()

    def LND_name():
        color_text.change_to_blue()
        LND_name = input(' Insert your LND node name ---> ')
        color_text.reset_style()
        return LND_name

    def LND_wallet_pass():
        color_text.change_to_blue()
        wallet_pass = getpass.getpass(prompt=' Insert Lightning wallet password (min 8 char) ---> ')
        if len(wallet_pass) < 8:
            color_text.warning('Password must be at least 8 characters')
            return user_input.LND_wallet_pass()
        else:
            confirm_pass = getpass.getpass(prompt=' Repeat Lightning wallet password ---> ')
            if user_input.check_password(wallet_pass, confirm_pass):
                return wallet_pass
            else:
                color_text.warning('Passwords don\'t match, try again.')
                return user_input.LND_wallet_pass()
        color_text.reset_style()

    def RTL_pass():
        color_text.change_to_blue()
        RTL_pass = getpass.getpass(prompt=' Insert RTL webui password ---> ')
        confirm_pass = getpass.getpass(prompt=' Repeat RTL webui password ---> ')
        color_text.reset_style()
        if user_input.check_password(RTL_pass, confirm_pass):
            return RTL_pass
        else:
            color_text.warning('Passwords don\'t match, try again.')
            return user_input.RTL_pass()
        color_text.reset_style()

    def tor_setup():  # mirar bien esta función más adelante
        color_text.change_to_blue()
        tor_setup = input(' Do you want to use TOR (y/n)  ---> ')
        if user_input.check_answer(tor_setup):
            if tor_setup.lower() in ['y']:
                return 'y'
            elif tor_setup.lower() in ['n']:
                return 'n'
        else:
            user_input.tor_setup()

    def download_chain():
        color_text.change_to_blue()
        download_chain = input(' Do you want to download the blockchain ---> ')
        if user_input.check_answer(download_chain):
            if download_chain.lower() in ['y']:
                return 'y'
            elif download_chain.lower() in ['n']:
                return 'n'
        else:
            user_input.download_chain()
